@extends('layout')
@section('content')
	<div class="row">
		<div class="col-md-6 offset-md-3">
			@if($message = Session::get('danger'))
				<div class="alert alert-danger">
					<strong>Update data failed, please try again</strong>
				</div>
			@endif
			@foreach($posts as $post)
			<form action="{{ action('PostController@update', $post->id) }}" method="post">
				@csrf
				@method('PUT')
				<div class="form-group">
					<label>Name</label>
					<input class="form-control" type="text" name="name" placeholder="Nama Lengkap" value="{{ $post->name }}">
				</div>
				<div class="form-group">
					<label>Detail</label>
					<textarea class="form-control" name="detail" placeholder="Detail">{{ $post->detail }}</textarea>
				</div>
				<div class="form-group">
					<label>Author</label>
					<input type="text" name="author" class="form-control" value="{{ $post->author }}">
				</div>
				<button class="btn btn-primary" type="submit">Update</button>
				<a href="{{ action('PostController@index') }}" class="btn btn-danger">Back</a>
			</form>
			@endforeach
		</div>
	</div>
@endsection