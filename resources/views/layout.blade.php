<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>CRUD Laravel 5.6</title>
	
	<link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">

</head>
<body>

	<div class="container">
		<p><br></p>
		@yield('content')
	</div>

</body>
</html>