@extends('layout')
@section('content')
	
	<div class="row">
		<div class="col-md-6 offset-md-3">
			@if($message = Session::get('danger'))
				<div class="alert alert-danger">
					<strong>Input data failed, please try again</strong>
				</div>
			@endif
			<form action="{{ action('PostController@store') }}" method="post">
				@csrf
				<div class="form-group">
					<label>Name</label>
					<input type="text" name="name" class="form-control" placeholder="Nama Lengkap">
				</div>
				<div class="form-group">
					<label>Detail</label>
					<textarea class="form-control" name="detail" placeholder="Detail"></textarea>
				</div>
				<div class="form-group">
					<label>Author</label>
					<input type="text" placeholder="Pemilik" name="author" class="form-control">
				</div>
				<button class="btn btn-primary" type="submit">Submit</button>
			</form>
		</div>
	</div>

@endsection