@extends('layout')

@section('content')
<!-- {{Session::get('success')}} -->
@if ($message = Session::get('success'))
	<div class="alert alert-success">
		<p>{{ $message }}</p>
	</div>
@endif

<div class="row">
	<div class="col-md-6">
		<h1>CRUD Laravel 5.6</h1>
	</div>
	<div class="col-md-6 text-right">
		<a href="{{ action('PostController@create') }}" class="btn btn-primary">Add Data</a>
	</div>
</div>
<table class="table table-bordered">
	<thead>
		<tr>
			<th>No</th>
			<th>Name</th>
			<th>Detail</th>
			<th>Author</th>
			<th width="210">Action</th>
		</tr>
	</thead>
	<tbody>
		@php
           $no = 0
        @endphp
		@foreach($posts as $post)
		<tr>
			<td>{{ ++$no }}</td>
			<td>{{ $post->name }}</td>
			<td>{{ $post->detail }}</td>
			<td>{{ $post->author }}</td>
			<td>
				<form action="{{ action('PostController@destroy', $post->id) }}" method="post">
					<a href="{{ action('PostController@show', $post->id) }}" class="btn btn-success">View</a>
					<a href="{{ action('PostController@edit', $post->id) }}" class="btn btn-warning">Edit</a>
					<button type="submit" class="btn btn-danger">Delete</button>
					@csrf
					@method('DELETE')
				</form>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
@endsection